import java.util.Scanner;

public class Main {
    public static void main (String [] args){
        int priceHome, firstContribution, revenue;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите цену квратиры");
        priceHome = scanner.nextInt();
        System.out.println("Введите сумму первого взноса");
        firstContribution = scanner.nextInt();
        System.out.println("Введите ежемесячный доход");
        revenue = scanner.nextInt();
        int  quantityYears = 5, persent=5;
        double sumCredit, monthlyPay;
        while (quantityYears<=25){
            switch (quantityYears){
                case 5:{
                    persent = 12;
                    break;
                }
                case 10:{
                    persent = 10;
                    break;
                }
                case 15:{
                    persent = 8;
                    break;
                }
                case 20:{
                    persent = 7;
                    break;
                }
                case 25:{
                    persent = 6;
                    break;
                }
                default:{
                    persent = 0;
                    System.out.println("error");
                    break;
                }
            }
            sumCredit = priceHome + (priceHome * quantityYears * persent) / 100;
            monthlyPay = sumCredit/(quantityYears*12);
            if (monthlyPay < revenue){
                System.out.println("Колличество лет = "+quantityYears);
                System.out.println("Ежемесячный платеж = " + monthlyPay);
                System.out.println("Сумма кредита = " + sumCredit);
            }
            quantityYears+=5;
        }
    }
}
